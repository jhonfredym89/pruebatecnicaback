package com.alfatecsistemas.sihna.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alfatecsistemas.sihna.bean.Empleado;
import com.alfatecsistemas.sihna.service.EmpleadoFacade;

@Controller
@RequestMapping("/empleado")
public class EmpleadoController {
	@Autowired
	private EmpleadoFacade empleadoFacade;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Empleado> guardar(@RequestBody Empleado empleado) {
		empleadoFacade.guardar(empleado);
		return new ResponseEntity<>(empleado, HttpStatus.OK);
	}

	@RequestMapping(value = "/findByDepartamento/{idDepartamento}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Empleado>> buscarPorDepartamento(@PathVariable int idDepartamento) {
		return new ResponseEntity<>(empleadoFacade.buscarPorDepartamento(idDepartamento), HttpStatus.OK);
	}

	@RequestMapping(value = "/findByIdAndDepartamento/{idEmpleado}/{idDepartamento}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Empleado> buscarPorIdAndDepartamento(@PathVariable int idEmpleado,
			@PathVariable int idDepartamento) {
		return new ResponseEntity<>(empleadoFacade.buscarPorIdAndDepartamento(idEmpleado, idDepartamento),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Empleado> eliminar(@RequestBody Empleado empleado) {
		empleadoFacade.eliminar(empleado);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Empleado> actualizar(@RequestBody Empleado empleado) {
		return new ResponseEntity<>(empleadoFacade.actualizar(empleado), HttpStatus.OK);
	}
}
