package com.alfatecsistemas.sihna.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alfatecsistemas.sihna.bean.Departamento;
import com.alfatecsistemas.sihna.service.DepartamentoFacade;

@Controller
@RequestMapping("/departamento")
public class DepartamentoController {

	@Autowired
	private DepartamentoFacade departamentoFacade;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Departamento> guardar(@RequestBody Departamento departamento) {
		departamentoFacade.guardar(departamento);
		return new ResponseEntity<>(departamento, HttpStatus.OK);
	}
}
