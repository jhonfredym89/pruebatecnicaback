package com.alfatecsistemas.sihna.repository;

import com.alfatecsistemas.sihna.bean.Departamento;

public interface DepartamentoRepository {
	void guardar(Departamento departamento);
}
