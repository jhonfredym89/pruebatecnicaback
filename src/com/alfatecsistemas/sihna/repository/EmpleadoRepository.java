package com.alfatecsistemas.sihna.repository;

import java.util.List;

import com.alfatecsistemas.sihna.bean.Empleado;

public interface EmpleadoRepository {
	void guardar(Empleado empleado);

	List<Empleado> buscarPorDepartamento(int idDepartamento);

	Empleado buscarPorIdAndDepartamento(int idEmpleado, int idDepartamento);

	void eliminar(Empleado empleado);

	Empleado actualizar(Empleado empleado);
}
