package com.alfatecsistemas.sihna.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alfatecsistemas.sihna.bean.Empleado;
import com.alfatecsistemas.sihna.repository.EmpleadoRepository;

@Repository
public class EmpleadoRepositoryImpl implements EmpleadoRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public void guardar(Empleado empleado) {
		entityManager.persist(empleado);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Empleado> buscarPorDepartamento(int idDepartamento) {
		final StringBuilder jpql = new StringBuilder();
		final Query query;

		jpql.append("SELECT e FROM Empleado e ");
		jpql.append("JOIN FETCH e.departamento d ");
		jpql.append("WHERE d.id = :idDepartamento ");

		query = entityManager.createQuery(jpql.toString());
		query.setParameter("idDepartamento", idDepartamento);

		return query.getResultList();
	}

	@Override
	public Empleado buscarPorIdAndDepartamento(int idEmpleado, int idDepartamento) {
		final StringBuilder jpql = new StringBuilder();
		final Query query;

		jpql.append("SELECT e FROM Empleado e ");
		jpql.append("JOIN FETCH e.departamento d ");
		jpql.append("WHERE e.id = :idEmpleado ");
		jpql.append("AND d.id = :idDepartamento ");

		query = entityManager.createQuery(jpql.toString());
		query.setParameter("idEmpleado", idEmpleado);
		query.setParameter("idDepartamento", idDepartamento);

		return (Empleado) query.getSingleResult();
	}

	@Override
	@Transactional
	public void eliminar(Empleado empleado) {
		entityManager.remove(entityManager.contains(empleado) ? empleado : entityManager.merge(empleado));
	}

	@Override
	@Transactional
	public Empleado actualizar(Empleado empleado) {
		return entityManager.merge(empleado);
	}
}
