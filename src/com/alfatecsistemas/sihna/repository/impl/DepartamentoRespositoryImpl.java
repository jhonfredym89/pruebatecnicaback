package com.alfatecsistemas.sihna.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alfatecsistemas.sihna.bean.Departamento;
import com.alfatecsistemas.sihna.repository.DepartamentoRepository;

@Repository
public class DepartamentoRespositoryImpl implements DepartamentoRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public void guardar(Departamento departamento) {
		entityManager.persist(departamento);
	}
}
