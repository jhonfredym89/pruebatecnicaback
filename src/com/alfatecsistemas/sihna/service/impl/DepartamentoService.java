package com.alfatecsistemas.sihna.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alfatecsistemas.sihna.bean.Departamento;
import com.alfatecsistemas.sihna.repository.DepartamentoRepository;
import com.alfatecsistemas.sihna.service.DepartamentoFacade;

@Service
public class DepartamentoService implements DepartamentoFacade {

	@Autowired
	private DepartamentoRepository departamentoRepository;

	@Override
	public void guardar(Departamento departamento) {
		departamentoRepository.guardar(departamento);
	}
}
