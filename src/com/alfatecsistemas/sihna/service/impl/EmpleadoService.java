package com.alfatecsistemas.sihna.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alfatecsistemas.sihna.bean.Empleado;
import com.alfatecsistemas.sihna.repository.EmpleadoRepository;
import com.alfatecsistemas.sihna.service.EmpleadoFacade;

@Service
public class EmpleadoService implements EmpleadoFacade {

	@Autowired
	private EmpleadoRepository empleadoRepository;

	@Override
	public void guardar(Empleado empleado) {
		empleadoRepository.guardar(empleado);
	}

	@Override
	public List<Empleado> buscarPorDepartamento(int idDepartamento) {
		return empleadoRepository.buscarPorDepartamento(idDepartamento);
	}

	@Override
	public Empleado buscarPorIdAndDepartamento(int idEmpleado, int idDepartamento) {
		return empleadoRepository.buscarPorIdAndDepartamento(idEmpleado, idDepartamento);
	}

	@Override
	public void eliminar(Empleado empleado) {
		empleadoRepository.eliminar(empleado);
	}

	@Override
	public Empleado actualizar(Empleado empleado) {
		return empleadoRepository.actualizar(empleado);
	}
}
