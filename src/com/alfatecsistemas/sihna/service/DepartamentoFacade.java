package com.alfatecsistemas.sihna.service;

import com.alfatecsistemas.sihna.bean.Departamento;

public interface DepartamentoFacade {
	void guardar(Departamento departamento);
}
